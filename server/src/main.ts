import { NestFactory } from '@nestjs/core';

import CONFIG from '@kroko-crm/config';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(CONFIG.PORT);
}
bootstrap();
